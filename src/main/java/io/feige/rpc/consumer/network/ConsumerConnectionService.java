package io.feige.rpc.consumer.network;

import io.feige.rpc.consumer.RpcConsumerService;
import io.feige.rpc.consumer.config.RemoteServiceConfig;
import io.feige.rpc.consumer.network.listener.InvokeListener;

import java.net.SocketAddress;
import java.util.Map;

public interface ConsumerConnectionService {

	void start(RpcConsumerService rpcConsumerService);
	
	void stop();
	
	void updateConnections(Map<String, RemoteServiceConfig> remoteServiceConfigMap);
	
	void connect(SocketAddress sa);
	
	void removeConnection(SocketAddress sa);
	
	boolean checkConnection(SocketAddress sa);
	
	Object write(SocketAddress sa, String service, String method, Class<?>[] types, Object[] args, int timeout);

	void write(SocketAddress sa, String service, String method, Class<?>[] types, Object[] args, InvokeListener<?> listener);

	void received(Object result, Throwable e, Long seq);
	
}
