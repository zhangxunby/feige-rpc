package io.feige.rpc.consumer.network.implement;

import static org.jboss.netty.channel.Channels.pipeline;
import io.feige.rpc.consumer.RpcConsumerService;
import io.feige.rpc.consumer.config.RemoteServiceConfig;
import io.feige.rpc.consumer.network.ConsumerConnectionService;
import io.feige.rpc.consumer.network.future.InvokeFuture;
import io.feige.rpc.consumer.network.listener.InvokeListener;
import io.feige.rpc.exception.RpcNetworkException;
import io.feige.rpc.protocol.Protocol;

import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelDownstreamHandler;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerConnectionServiceImpl implements ConsumerConnectionService {

	private static final Logger logger=LoggerFactory.getLogger(ConsumerConnectionServiceImpl.class);
	
	private ClientBootstrap bootstrap; 
	
	private HashedWheelTimer idleTimer = new HashedWheelTimer(); 
	
	private AtomicLong seq=new AtomicLong(0);
	
	private Protocol protocol;
	//现在每个服务只建立一个连接，满足大多数情况，后继实现连接池
	private Map<String, Channel> channels=new ConcurrentHashMap<String, Channel>();

	public Map<Long, InvokeFuture<Object>> futrues=new ConcurrentHashMap<Long, InvokeFuture<Object>>();
 
	private RpcConsumerService rpcConsumerService;
	
	@Override
	public void start(RpcConsumerService rpcConsumerService) {
		this.rpcConsumerService=rpcConsumerService;
		this.protocol=rpcConsumerService.getProtocol();
        bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        bootstrap.setPipelineFactory(new ClientPipelineFactory()); 
        updateConnections(rpcConsumerService.getRemoteServiceConfigManager().getRemoteServiceConfigMap());
	}

	@Override
	public void stop() {
		bootstrap.releaseExternalResources();
	}
	
	@Override
	public void updateConnections(Map<String, RemoteServiceConfig> remoteServiceConfigMap) {
		
		//后继会实现配置动态刷新
		Map<String, SocketAddress> saMap=new HashMap<String, SocketAddress>();
		Iterator<String> ite=remoteServiceConfigMap.keySet().iterator();
		while(ite.hasNext()){
			String key=ite.next();
			RemoteServiceConfig config=remoteServiceConfigMap.get(key);
			for (SocketAddress socketAddress : config.getHosts()) {
				saMap.put(socketAddress.toString(), socketAddress);
				if (!channels.containsKey(socketAddress.toString())) {
					connect(socketAddress);
				}
			}
		}
		
		Iterator<String> cite = channels.keySet().iterator();
		while(cite.hasNext()){
			String key=cite.next();
			if(!saMap.containsKey(key)){
				removeChannels(key);
			}
		}

	}

	private void removeChannels(String key) {
		channels.get(key).close();
	}

	@Override
	public void connect(SocketAddress sa) {
		ChannelFuture future = bootstrap.connect(sa);
		future.addListener(new ConnectListener());
	}
 
	public class ConnectListener implements ChannelFutureListener {
        
	    public void operationComplete(ChannelFuture future){
	        if (!future.isSuccess()) {                                                  
	        	logger.warn("connect failed, {}", future.getCause().getMessage());                             
	            return;                                                                 
	        }
	        Channel channel = future.getChannel();
	        channels.put(channel.getRemoteAddress().toString(), channel);
	    }
	}
	 
	@Override
	public void removeConnection(SocketAddress sa) {
		if (sa!=null) {
			channels.remove(sa.toString());
		} 
	}

	@Override
	public boolean checkConnection(SocketAddress sa) {
		Channel channel = channels.get(sa.toString());
		return channel!=null&&channel.isWritable();
	}

	@Override
	public Object write(SocketAddress sa, String service, String method, Class<?>[] types, Object[] args, int timeout) {
		Long seq=getSeq();
		Object obj=protocol.getWriteObject(service, method, types, args, seq);
		InvokeFuture<Object> future=new InvokeFuture<Object>();
		futrues.put(seq, future);
		getChannel(sa).write(obj);
		try {
			Object result=future.getResult(timeout, TimeUnit.SECONDS);
			return result;
		} catch (RuntimeException e) {
			throw e;
		}finally {
			futrues.remove(seq);
		}
	}
	
	@Override
	public void write(SocketAddress sa, String service, String method, Class<?>[] types, Object[] args, InvokeListener<?> listener) {
		
	}
	
	private Channel getChannel(SocketAddress sa){
		Channel channel=channels.get(sa.toString());
		if (channel==null||!channel.isReadable()) {
			throw new RpcNetworkException();
		}
		return channel;
	}
	
	private Long getSeq(){
		return seq.getAndIncrement();
	}
	
	public class ClientPipelineFactory implements ChannelPipelineFactory {
		 

		public ChannelPipeline getPipeline() throws Exception {
	        ChannelPipeline pipeline = pipeline();
	        pipeline.addLast("decoder", (SimpleChannelUpstreamHandler)protocol.getDecoder());
	        pipeline.addLast("encoder", (ChannelDownstreamHandler)protocol.getEncoder());
	        pipeline.addLast("timeout", new IdleStateHandler(idleTimer, 15, 10, 0));
			pipeline.addLast("idleHandler", (IdleStateAwareChannelHandler)protocol.getStateCheckChannelHandler(rpcConsumerService)); 
	        pipeline.addLast("handler", (SimpleChannelHandler)protocol.getInvokerResponseHandler(rpcConsumerService));
	        return pipeline;
	    }
	}

	@Override
	public void received(Object result, Throwable e, Long seq) {
		if (seq!=null) {
			InvokeFuture<Object> future = futrues.remove(seq);
			if (future==null) {
				return;
			}
			if (e!=null) {
				future.setCause(e);
			}else{
				future.setResult(result);
			}
		}
	}
 
}
