package io.feige.rpc.consumer.cluster;

import io.feige.rpc.consumer.config.ServiceSocketAddress;

import java.util.List;

public interface LoadBalance {

	ServiceSocketAddress select(List<ServiceSocketAddress> socketAddresses);
	
}
