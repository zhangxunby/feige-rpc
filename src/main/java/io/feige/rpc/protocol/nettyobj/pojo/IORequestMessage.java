package io.feige.rpc.protocol.nettyobj.pojo;

import java.io.Serializable;
import java.util.Arrays;

public class IORequestMessage implements Serializable {
 
	private static final long serialVersionUID = 7391777630120983536L;
 
	private String service;
	
	private String method;
	
	private Class<?>[] types;
	
	private Object[] args;

	private long seq;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}

	@Override
	public String toString() {
		return "IORequestMessage [service=" + service + ", method=" + method
				+ ", args=" + Arrays.toString(args) + ", seq=" + seq + "]";
	}

	public Class<?>[] getTypes() {
		return types;
	}

	public void setTypes(Class<?>[] types) {
		this.types = types;
	}
	
}
