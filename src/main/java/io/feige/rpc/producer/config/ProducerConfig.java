package io.feige.rpc.producer.config;

import io.feige.rpc.protocol.Protocol;
import io.feige.rpc.protocol.nettyobj.NettyObjectProtocol;


public class ProducerConfig {

	private String host;
	
	private Protocol protocol;
	
	private int port;
	
	private int poolSize;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public Protocol getProtocol() {
		return protocol==null?protocol=new NettyObjectProtocol():protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}
	
	
	
}
